

const server_address="https://mini-app-client.herokuapp.com"
// const server_address="http://127.0.0.1:8080"
const CLIENT_ID="32246790-c1da-11eb-8273-0242ac1200ad";
// const CLIENT_ID="abf8fc30-caa8-11eb-bfe4-0242ac1200de";
let miniapp=new hamromini_sdkjs.MiniApp()



 async function initializer(){
    try{
        document.getElementById("init-button").innerHTML="Initializing..." 
        let userDetails=await miniapp.init(CLIENT_ID);
        displayLog(userDetails,'From init') 
        
    }catch(err){
        displayLog(err,'From init') 
        // alertify.alert('the user details cannot be fetched during initialization!')
    }   
    document.getElementById("init-button").innerHTML="Init" 
}



async function userClientLogin(){
    try{     
        document.getElementById("login-button").innerHTML="Logging in..." 
        
        
            let userDetails=await miniapp.login('google')
            if (userDetails){
                console.log(userDetails)
                displayLog(userDetails,'From login')
            }           
    }catch(err){
        displayLog(err,'From login')
    }
    document.getElementById("login-button").innerHTML="Log in" 
}
async function AccessToken(){
    try{     
        document.getElementById("access-button").innerHTML="Accessing..." 
            let token=await miniapp.getAccessToken()
            displayLog(token,'From AccessToken')            
    }catch(err){
        displayLog(err,'From AccessToken')
    }
    document.getElementById("access-button").innerHTML="Get Access Token" 
}

async function userProfile(){
    try{     
        document.getElementById("userprofile-button").innerHTML="Fetching..." 
            let profile=await miniapp.getUserProfile()
            displayLog(profile,'From getUserProfile')            
    }catch(err){
        displayLog(err,'From getUserProfile')
    }
    document.getElementById("userprofile-button").innerHTML="Get User Profile"
}

async function getSdkVersion(){
    try{
       let result= await miniapp.getSdkVersion()
       displayLog(result,'From getSdkVersion')
    }catch(err){
       displayLog(err,'From getSdkVersion')
    }   
}



 async function checkLoginStatus(){
     try{
        let result= await miniapp.isUserLoggedIn()
        displayLog(result,'From getLoginStatus')
     }catch(err){
        displayLog(err,'From getLoginStatus')
     }   
 }

 async function getAppPlatform(){
    try{
       let result= await miniapp.getPlatform()
       displayLog(result,'From getPlatform')
    }catch(err){
       displayLog(err,'From getPlatform')
    }   
}


 function displayLog(message,functionName){
    if( typeof displayLog.logEntryNumber == 'undefined' ) {
        displayLog.logEntryNumber = 0;
     }
    let displayParentElement=document.getElementById('consoleLog')
    let messageHeader=document.createElement('h2')
    let newMessageElement=document.createElement('pre')
    let lineRuler=document.createElement('hr')
    newMessageElement.innerHTML=JSON.stringify(message,undefined,4)
    messageHeader.innerText=`Log #${displayLog.logEntryNumber}:${functionName}()`
    displayParentElement.prepend(newMessageElement)
    displayParentElement.prepend(messageHeader)
    displayParentElement.prepend(lineRuler)
    displayLog.logEntryNumber++
 }

 