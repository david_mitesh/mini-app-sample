const server_address="https://mini-app-client.herokuapp.com"
// const server_address="http://127.0.0.1:8080"
const CLIENT_ID="abf8fc30-caa8-11eb-bfe4-0242ac1200de";
// const CLIENT_ID="6caff600-b7ca-11eb-baa9-0242ac1200aa";

//creating an instance of MiniApp class
let miniapp=new hamromini_sdkjs.MiniApp()



 async function initializer(){
    try{
       
        let userDetails=await hamromini_sdkjs.initialize(CLIENT_ID,miniapp);
        // console.log(`from initializer ${userDetails}`)
    if (userDetails.user_profile){
        userProfileDisplay(userDetails)
    }
    }catch(err){
        console.log(`from error ${err}`)
        alertify.alert('the user details cannot be fetched during initialization!')
    }   

    document.getElementById("loader").style.display = "none";

}
window.onload=initializer()


async function userClientLogin(signin_provider){
    try{     
        document.getElementById(`login-button-${signin_provider}`).innerHTML="Logging in..." 
        let userDetails=await miniapp.login(signin_provider)
        userProfileDisplay(userDetails)
                    
    }catch(err){
        alertify.alert('Error', JSON.stringify(err) )
        document.getElementById(`login-button-${signin_provider}`).innerHTML="Log in" 
    }
    
}

 async function checkout(){
        try{
            document.getElementById("checkout-button").innerHTML="Processing..." 
            if (! await miniapp.isUserLoggedIn()){
                alertify.confirm('Login to checkout', 'Click "ok" to login and process checkout.', async function(){ 
                    await miniapp.login()
                    checkout()
                }
                , function(){ 
                    document.getElementById("checkout-button").innerHTML="Checkout"
                    alertify.error('Action cannot be completed!')
                })
            }else{
                console.log("from checkout else block")
                let checkoutRequest = {
                    "url": `${server_address}/checkout`,
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                    "accessToken": await miniapp.getAccessToken(),
                    },
                };  
                console.log('success') 
                $.ajax({...checkoutRequest,
                success : function(data){
                    let profile=JSON.parse(localStorage.getItem("mini-app-profile"))
                    if (profile){
                        userProfileDisplay(profile)
                    }
                    console.log(data)
                    alertify.success('Order confirmed!')
                    document.getElementById("checkout-button").innerHTML="Checkout"
                    document.getElementById("order-confirmation").innerHTML="Order successfully placed!"
                },
                error: async function(xhr, status, error){
                    if(xhr.status === 0) {
                        alertify.confirm( 'Could not connect to the server. click "ok" to retry.', function(){ checkout() },function(){});
                }else if (xhr.status === 401){
                    
                    document.getElementById("checkout-button").innerHTML="Checkout"
                }else{
                    alertify.confirm( 'Could not connect to the server. click "ok" to retry.', function(){ checkout() },function(){
                        document.getElementById("checkout-button").innerHTML="Checkout"
                    });
                }  
                document.getElementById("checkout-button").innerHTML="Checkout"
                }
              })
            }
    }catch(err){
        console.log(err)
        alertify.alert(`Error encountered ! some error occured!` )
    }
    // document.getElementById("checkout-button").innerHTML="Checkout"
}

 function userProfileDisplay(profile){
    //  console.log('from user profile display')
    // console.log(profile)
    document.getElementById('login-details').innerHTML=
    `<h2>My Profile</h2>
    <div class="card">
    <img src=${profile.user_profile.photo_url} style="width:20%">
    <h1>${profile.user_profile.display_name}</h1>
    <p>${profile.user_profile.id}</p> 
    <p>${profile.user_profile.email}</p>  
    
  </div>
  <button onClick="logout()">Logout</button>
  <br />
  `
 }

 async function logout(){
     try{
        await miniapp.logout()
        location.reload()
     }catch(err){
        alertify.alert(JSON.stringify(err))
     }
     
 }