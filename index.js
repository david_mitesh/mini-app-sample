require('dotenv').config()
const {sendNotification,verifyToken}=require('hamro_service_message_sdk')
var express=require('express');

let cors=require('cors');
const path=require('path')


//some constants


const LISTEN_PORT=process.env.PORT||8080

var app=express();

//use of middlewares
app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.static(path.join(__dirname, 'public')))



/**
 * API End-points
 *  */ 
app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname, 'public/index.html'))
})

app.get('/login',(req,res)=>{
    res.sendFile(path.join(__dirname, 'public/login.html'))
})

//This API call can operate without login(login not required)
app.get('/getItems',(req,res)=>{
    res.send({
        name:"sofa",
        price:4000
    })
})

//This api call is for checkout of items where login is required.So, verifyToken middleware is invoked.
app.post('/checkout',verifyToken,async (req,res)=>{
    let userDetail=req.tokenPayload
    // console.log(userDetail.sub)

    try{
        var message=getServiceMessage()
        let details=await sendNotification({...message,miniAppUserId:req.tokenPayload.sub})
        res.status(200).send(details)
      }catch(err){
          console.log('from error block in checkout')
          console.log(err)
          /**
           * handle this error to retry
           */
          
      }
})


app.get('/test',(req,res)=>{
  res.sendFile(path.join(__dirname, 'public/indextest.html'))
})

//listening port specifications
app.listen(LISTEN_PORT,()=>{
    console.log(`listening to port ${LISTEN_PORT}`)
})

function getServiceMessageId() {
    // Modeled after base64 web-safe chars, but ordered by ASCII.
    var PUSH_CHARS = '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
  
    // Timestamp of last push, used to prevent local collisions if you push twice in one ms.
    var lastPushTime = 0;
  
    // We generate 72-bits of randomness which get turned into 12 characters and appended to the
    // timestamp to prevent collisions with other clients.  We store the last characters we
    // generated because in the event of a collision, we'll use those same characters except
    // "incremented" by one.
    var lastRandChars = [];
  
    return function() {
      var now = new Date().getTime();
      var duplicateTime = (now === lastPushTime);
      lastPushTime = now;
  
      var timeStampChars = new Array(8);
      for (var i = 7; i >= 0; i--) {
        timeStampChars[i] = PUSH_CHARS.charAt(now % 64);
        // NOTE: Can't use << here because javascript will convert to int and lose the upper bits.
        now = Math.floor(now / 64);
      }
      if (now !== 0) throw new Error('We should have converted the entire timestamp.');
  
      var id = timeStampChars.join('');
  
      if (!duplicateTime) {
        for (i = 0; i < 12; i++) {
          lastRandChars[i] = Math.floor(Math.random() * 64);
        }
      } else {
        // If the timestamp hasn't changed since last push, use the same random number, except incremented by 1.
        for (i = 11; i >= 0 && lastRandChars[i] === 63; i--) {
          lastRandChars[i] = 0;
        }
        lastRandChars[i]++;
      }
      for (i = 0; i < 12; i++) {
        id += PUSH_CHARS.charAt(lastRandChars[i]);
      }
      if(id.length != 20) throw new Error('Length should be 20.');
  
      return id;
    };
  }

function getServiceMessage(){
  var serviceMessageId=getServiceMessageId()()
  // console.log(serviceMessageId)
  var messageReq={
    templateName: "order_confirmed", //Required field
    serviceMessageId, //Required Field
    //miniAppUserId is required field. See the /checkout endpoint.Implemented there.
    data:{
        details: {
            "order_no":"1",
            
       },
        buttons: 
            {
                "view_order":"https://google.com"
                
            }
        ,
        carousel: 
            {
                size:1,
                items:[{
                    "name": "test image",
                    "url": "https://www.youtube.com/watch?v=9em32dDnTck",
                    "itemType": "VIDEO",
                    "thumbnail": "https://lh3.googleusercontent.com/a/AATXAJzyar-TVwg7iJq6ARXG6iIyigPEV4NjH8DCrGvK=s96-c",
                    "caption": "test image",
                    "deeplink": "https://www.google.com",
                    "aspectRatio": 1.777778,
                }],
            },
        text:"This is Lorem Epsum text",
            },
    }
    return messageReq
}


 




  /**
   * 
   * 
   * To do
   * - fix login DONE
   * - change auth and service mesage urls for production
   * - implment getHostVersiona and getSdkVersion
   * - deploy both sdk in hamrostack
   * - give deployed url to gift
   */